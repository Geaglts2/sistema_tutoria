const carreras = require("../carreras");

exports.seed = function(knex) {
    return knex("carrera")
        .del()
        .then(function() {
            return knex("carrera").insert(carreras);
        });
};

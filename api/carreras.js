const express = require("express");

const router = express.Router();

const queries = require("../db/queries");

router.get("/", async (req, res) => {
    const carreras = await queries.getAll();
    res.json(carreras);
});

module.exports = router;

const knex = require("../db/knex");

describe("CRUD carrera", () => {
    before(async () => {
        //Run migrations
        knex.migrate.latest().then(() => {
            //Run seeds
            return knex.seed.run();
        });
    });
});
